import os
import time
import hashlib
import collections

import sublime
import sublime_plugin


try:
    from floo.common import msg, shared as G, utils
    from floo.sublime_utils import get_buf, get_text
    assert G and G and utils and msg and get_buf and get_text
except ImportError:
    from floo.common import msg, shared as G, utils
    from floo.sublime_utils import get_buf, get_text


def if_connected(f):
    def wrapped(*args):
        if not G.AGENT or not G.AGENT.is_ready():
            return
        args = list(args)
        return f(*args)
    return wrapped

def if_not_connected(f):
    def wrapped(*args):
        if not G.AGENT or not G.AGENT.is_ready():
            args = list(args)
            return f(*args)
        return
    return wrapped

def is_floodir(f):
    def wrapped(*args):
        if not G.AGENT or not G.AGENT.is_ready():
            return
        args = list(args)
        return f(*args)
    return wrapped


class JoinListener(sublime_plugin.EventListener):
    def __init__(self):
        super(JoinListener, self).__init__()
        self.displaymsg = False
        self.windows_displayed=[]
        self.msgcount=0
        self.message_isremoved=False
        self.message_on_all_open=False

    def is_floodir(self, view):
        if not hasattr(view.window(), 'folders'):
            return
        for d in view.window().folders():
            floo_file = os.path.join(d, '.floo')
            if os.path.isfile(floo_file):
                return True
        return False

    def didshow_for_window(self,view):
        return bool(view.window() in self.windows_displayed)

    def set_notcon_warning(self, view):
        view.set_status("flooconstat", "NOT CONNECTED")

    def set_warning(self,view):
        self.message_isremoved=False
        if self.message_on_all_open:
            self.set_notcon_warning(view)
        else:
            for v in view.window().views():
                self.set_notcon_warning(v)

    # def join_check(self,view):
    #     self.message_isremoved=False
    #     if not self.is_floodir(view):
    #         return
    #     if not self.message_on_all_open:
    #         self.message_on_all_open=True
    #         self.set_warning(view)
    #     else:
    #         self.set_notcon_warning(view)
    #         # self.windows_displayed.append(view.window())
    #         # not_joined_message=u"Du har glemt joining!"
    #         #sublime.message_dialog(not_joined_message)
            
    def clear_warning(self, view):
        self.message_on_all_open=False
        if self.message_isremoved:
            return
        self.message_isremoved=True
        for v in view.window().views():
            v.erase_status("flooconstat")

    def is_connected(self):
        return bool(G.AGENT and G.AGENT.is_ready())
    
    def join_check(self,view):
        connected=self.is_connected()
        floodir=self.is_floodir(view)
        if not connected and floodir:
            if view.file_name()!=None:
                self.set_warning(view)
                view.set_read_only(True)
        elif connected and floodir:
            if view.file_name()!=None:
                view.set_read_only(False)
                self.clear_warning(view)

    def on_modified(self,view):
        return self.join_check(view)
    def on_activated(self, view):
        return self.join_check(view)

    # @if_connected
    # def on_activated(self, view):
    #     print("ACTIVE CON")
    #     return self.clear_warning(view)
    
    # @if_connected       
    # def on_post_save(self, view):
    #     print("MOD CON")
    #     return self.clear_warning(view)
    
    # @if_not_connected
    # def on_load(self,view):
    #     return self.join_check(view)

    # @if_not_connected
    # def on_post_save(self, view):
    #     return self.join_check(view)
    
    # @if_not_connected
    # def on_new(self, view):
    #     return self.join_check(view)
    
    # @if_not_connected
    # def on_activated(self, view):
    #     if not self.is_floodir:
    #         return
    #     if self.is_connected():
    #         return self.clear_warning(view)
    #     else:
    #         self.join_check(view)